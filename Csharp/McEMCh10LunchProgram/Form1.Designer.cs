﻿namespace McEMCh10LunchProgram
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpMainCourse = new System.Windows.Forms.GroupBox();
            this.rdbSalad = new System.Windows.Forms.RadioButton();
            this.rdbPizza = new System.Windows.Forms.RadioButton();
            this.rdbHamburger = new System.Windows.Forms.RadioButton();
            this.grpAddOnItems = new System.Windows.Forms.GroupBox();
            this.chkThird = new System.Windows.Forms.CheckBox();
            this.chkSecond = new System.Windows.Forms.CheckBox();
            this.chkFirst = new System.Windows.Forms.CheckBox();
            this.grpOrderTotal = new System.Windows.Forms.GroupBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.txtTax = new System.Windows.Forms.TextBox();
            this.txtSubtotal = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPlaceOrder = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.grpMainCourse.SuspendLayout();
            this.grpAddOnItems.SuspendLayout();
            this.grpOrderTotal.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpMainCourse
            // 
            this.grpMainCourse.Controls.Add(this.rdbSalad);
            this.grpMainCourse.Controls.Add(this.rdbPizza);
            this.grpMainCourse.Controls.Add(this.rdbHamburger);
            this.grpMainCourse.Location = new System.Drawing.Point(85, 39);
            this.grpMainCourse.Name = "grpMainCourse";
            this.grpMainCourse.Size = new System.Drawing.Size(214, 135);
            this.grpMainCourse.TabIndex = 0;
            this.grpMainCourse.TabStop = false;
            this.grpMainCourse.Text = "Main Course";
            // 
            // rdbSalad
            // 
            this.rdbSalad.AutoSize = true;
            this.rdbSalad.Location = new System.Drawing.Point(20, 93);
            this.rdbSalad.Name = "rdbSalad";
            this.rdbSalad.Size = new System.Drawing.Size(106, 21);
            this.rdbSalad.TabIndex = 2;
            this.rdbSalad.TabStop = true;
            this.rdbSalad.Text = "Salad-$4.95";
            this.rdbSalad.UseVisualStyleBackColor = true;
            this.rdbSalad.CheckedChanged += new System.EventHandler(this.RadioButton_Changed);
            // 
            // rdbPizza
            // 
            this.rdbPizza.AutoSize = true;
            this.rdbPizza.Location = new System.Drawing.Point(20, 65);
            this.rdbPizza.Name = "rdbPizza";
            this.rdbPizza.Size = new System.Drawing.Size(104, 21);
            this.rdbPizza.TabIndex = 1;
            this.rdbPizza.TabStop = true;
            this.rdbPizza.Text = "Pizza-$5.95";
            this.rdbPizza.UseVisualStyleBackColor = true;
            this.rdbPizza.CheckedChanged += new System.EventHandler(this.RadioButton_Changed);
            // 
            // rdbHamburger
            // 
            this.rdbHamburger.AutoSize = true;
            this.rdbHamburger.Location = new System.Drawing.Point(20, 37);
            this.rdbHamburger.Name = "rdbHamburger";
            this.rdbHamburger.Size = new System.Drawing.Size(141, 21);
            this.rdbHamburger.TabIndex = 0;
            this.rdbHamburger.TabStop = true;
            this.rdbHamburger.Text = "Hamburger-$6.95";
            this.rdbHamburger.UseVisualStyleBackColor = true;
            this.rdbHamburger.CheckedChanged += new System.EventHandler(this.RadioButton_Changed);
            // 
            // grpAddOnItems
            // 
            this.grpAddOnItems.Controls.Add(this.chkThird);
            this.grpAddOnItems.Controls.Add(this.chkSecond);
            this.grpAddOnItems.Controls.Add(this.chkFirst);
            this.grpAddOnItems.Location = new System.Drawing.Point(359, 39);
            this.grpAddOnItems.Name = "grpAddOnItems";
            this.grpAddOnItems.Size = new System.Drawing.Size(280, 135);
            this.grpAddOnItems.TabIndex = 1;
            this.grpAddOnItems.TabStop = false;
            this.grpAddOnItems.Text = "Add-on items";
            // 
            // chkThird
            // 
            this.chkThird.AutoSize = true;
            this.chkThird.Location = new System.Drawing.Point(22, 93);
            this.chkThird.Name = "chkThird";
            this.chkThird.Size = new System.Drawing.Size(98, 21);
            this.chkThird.TabIndex = 2;
            this.chkThird.Text = "checkBox1";
            this.chkThird.UseVisualStyleBackColor = true;
            this.chkThird.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // chkSecond
            // 
            this.chkSecond.AutoSize = true;
            this.chkSecond.Location = new System.Drawing.Point(22, 65);
            this.chkSecond.Name = "chkSecond";
            this.chkSecond.Size = new System.Drawing.Size(98, 21);
            this.chkSecond.TabIndex = 1;
            this.chkSecond.Text = "checkBox1";
            this.chkSecond.UseVisualStyleBackColor = true;
            this.chkSecond.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // chkFirst
            // 
            this.chkFirst.AutoSize = true;
            this.chkFirst.Location = new System.Drawing.Point(22, 37);
            this.chkFirst.Name = "chkFirst";
            this.chkFirst.Size = new System.Drawing.Size(98, 21);
            this.chkFirst.TabIndex = 0;
            this.chkFirst.Text = "checkBox1";
            this.chkFirst.UseVisualStyleBackColor = true;
            this.chkFirst.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // grpOrderTotal
            // 
            this.grpOrderTotal.Controls.Add(this.txtTotal);
            this.grpOrderTotal.Controls.Add(this.txtTax);
            this.grpOrderTotal.Controls.Add(this.txtSubtotal);
            this.grpOrderTotal.Controls.Add(this.label3);
            this.grpOrderTotal.Controls.Add(this.label2);
            this.grpOrderTotal.Controls.Add(this.label1);
            this.grpOrderTotal.Location = new System.Drawing.Point(85, 205);
            this.grpOrderTotal.Name = "grpOrderTotal";
            this.grpOrderTotal.Size = new System.Drawing.Size(385, 141);
            this.grpOrderTotal.TabIndex = 2;
            this.grpOrderTotal.TabStop = false;
            this.grpOrderTotal.Text = "Order Total";
            // 
            // txtTotal
            // 
            this.txtTotal.Location = new System.Drawing.Point(118, 83);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.Size = new System.Drawing.Size(120, 22);
            this.txtTotal.TabIndex = 5;
            // 
            // txtTax
            // 
            this.txtTax.Location = new System.Drawing.Point(118, 54);
            this.txtTax.Name = "txtTax";
            this.txtTax.ReadOnly = true;
            this.txtTax.Size = new System.Drawing.Size(120, 22);
            this.txtTax.TabIndex = 4;
            // 
            // txtSubtotal
            // 
            this.txtSubtotal.Location = new System.Drawing.Point(118, 24);
            this.txtSubtotal.Name = "txtSubtotal";
            this.txtSubtotal.ReadOnly = true;
            this.txtSubtotal.Size = new System.Drawing.Size(120, 22);
            this.txtSubtotal.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Order Total:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tax (7.75%):";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Subtotal:";
            // 
            // btnPlaceOrder
            // 
            this.btnPlaceOrder.Location = new System.Drawing.Point(512, 217);
            this.btnPlaceOrder.Name = "btnPlaceOrder";
            this.btnPlaceOrder.Size = new System.Drawing.Size(127, 46);
            this.btnPlaceOrder.TabIndex = 3;
            this.btnPlaceOrder.Text = "Place Order";
            this.btnPlaceOrder.UseVisualStyleBackColor = true;
            this.btnPlaceOrder.Click += new System.EventHandler(this.btnPlaceOrder_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(512, 288);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(127, 46);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 396);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnPlaceOrder);
            this.Controls.Add(this.grpOrderTotal);
            this.Controls.Add(this.grpAddOnItems);
            this.Controls.Add(this.grpMainCourse);
            this.Name = "Form1";
            this.Text = "Lunch Program";
            this.grpMainCourse.ResumeLayout(false);
            this.grpMainCourse.PerformLayout();
            this.grpAddOnItems.ResumeLayout(false);
            this.grpAddOnItems.PerformLayout();
            this.grpOrderTotal.ResumeLayout(false);
            this.grpOrderTotal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpMainCourse;
        private System.Windows.Forms.RadioButton rdbSalad;
        private System.Windows.Forms.RadioButton rdbPizza;
        private System.Windows.Forms.RadioButton rdbHamburger;
        private System.Windows.Forms.GroupBox grpAddOnItems;
        private System.Windows.Forms.CheckBox chkThird;
        private System.Windows.Forms.CheckBox chkSecond;
        private System.Windows.Forms.CheckBox chkFirst;
        private System.Windows.Forms.GroupBox grpOrderTotal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.TextBox txtTax;
        private System.Windows.Forms.TextBox txtSubtotal;
        private System.Windows.Forms.Button btnPlaceOrder;
        private System.Windows.Forms.Button btnExit;
    }
}

