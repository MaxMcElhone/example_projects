﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/*
 * Name: Maxwell McElhone
 * Date: 3/23/2018
 * Program: Lunch Program
 */
namespace McEMCh10LunchProgram
{
    public partial class Form1 : Form
    {

        //Class Level Variables
        double subtotal = 0;
        
        double addOnPrice = 0;
        
        public Form1()
        {
            InitializeComponent();
        }


        private void RadioButton_Changed(object sender, EventArgs e)
        {
            //Clearing everything
            chkFirst.Checked = false;
            chkSecond.Checked = false;
            chkThird.Checked = false;

            //Clearing TextBoxes
            ClearTextBoxes();

            if (rdbHamburger.Checked)
            {
                //price of main course
                subtotal = 6.95;

                grpAddOnItems.Text = "Add-on Items ($.75/each)";
                addOnPrice = .75;

                //set the names of the add-ons 
                chkFirst.Text = "Lettuce, tomato, and onions";
                chkSecond.Text = "Ketchup, mustard, and mayo";
                chkThird.Text = "French fries";
            }
            else if (rdbPizza.Checked)
            {
                //price of main course
                subtotal = 5.95;

                grpAddOnItems.Text = "Add-on Items ($.50/each)";
                addOnPrice = .50;

                //set the names of the add-ons 
                chkFirst.Text = "Pepperoni";
                chkSecond.Text = "Sausage";
                chkThird.Text = "Olives";
            }
            else
            {
                //price of main course
                subtotal = 4.95;

                grpAddOnItems.Text = "Add-on Items ($.25/each)";
                addOnPrice = .25;

                //set the names of the add-ons 
                chkFirst.Text = "Croutons";
                chkSecond.Text = "Bacon bits";
                chkThird.Text = "Bread sticks";
            }
        }

        private void CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            //get the control that caused the event
            var checkbox = (CheckBox)sender;

            //find if its changed to true or false
            //(Checked or Unchecked)
            //Then change subtotal accordingly
            if (checkbox.Checked)
            {
                subtotal += addOnPrice;
            }
            else
            {
                subtotal -= addOnPrice;
            }

            //Clearing TextBoxes
            ClearTextBoxes();
        }

        private void btnPlaceOrder_Click(object sender, EventArgs e)
        {

            //Calculate the total, tax, and subtotal.
            //Then output it as Currency
            const double TAX = .0775;
            txtSubtotal.Text = subtotal.ToString("C");
            txtTax.Text = (subtotal * TAX).ToString("C");
            txtTotal.Text = (subtotal * (1 + TAX)).ToString("C");
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ClearTextBoxes()
        {
            txtSubtotal.Clear();
            txtTax.Clear();
            txtTotal.Clear();
        }
    }
}
