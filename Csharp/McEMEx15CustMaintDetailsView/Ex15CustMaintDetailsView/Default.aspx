﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Ch15ProductMaintenance.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Ch15: Product Maintenance</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/site.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <header class="jumbotron"><%-- image set in site.css --%></header>
    <main class="row">
        <form id="form1" runat="server">

            <div class="col-sm-5 table-responsive">
                <asp:GridView ID="grdCustomers" runat="server" 
                    AutoGenerateColumns="False" DataKeyNames="Email" 
                    DataSourceID="SqlDataSource1" AllowPaging="True" 
                    SelectedIndex="0" UseAccessibleHeader="True" 
                    CssClass="table table-bordered table-striped table-condensed" 
                    OnPreRender="grdCustomers_PreRender">
                    <Columns>
                        <asp:BoundField DataField="Email" HeaderText="Email" 
                            ReadOnly="True" Visible="false">
                        </asp:BoundField>
                        <asp:BoundField DataField="LastName" HeaderText="Last Name">
                            <ItemStyle CssClass="col-xs-5" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FirstName" HeaderText="First Name"> 
                            <ItemStyle CssClass="col-xs-5" />
                        </asp:BoundField>
                        <asp:CommandField CausesValidation="false" ShowSelectButton="true">
                            <ItemStyle CssClass="col-xs-1" />
                        </asp:CommandField>
                    </Columns>
                    <HeaderStyle CssClass="bg-halloween" />
                    <PagerSettings Mode="NextPreviousFirstLast" />
                    <PagerStyle CssClass="pagerStyle" 
                        BackColor="#8c8c8c" HorizontalAlign="Center" />
                    <SelectedRowStyle CssClass="warning" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:HalloweenConnection %>" 
                    SelectCommand="SELECT [Email], [LastName], [FirstName] 
                        FROM [Customers] ORDER BY [LastName]">
                </asp:SqlDataSource>    
            </div>  

            <div class="col-sm-7">
                <asp:DetailsView ID="dvCustomer" runat="server" CssClass="table table-bordered table-condensed" AutoGenerateRows="False" DataKeyNames="Email" DataSourceID="SqlDataSource2" OnPageIndexChanging="dvCustomer_PageIndexChanging" BackColor="#E7E7E7" OnItemDeleted="dvCustomer_ItemDeleted" OnItemDeleting="dvCustomer_ItemDeleting" OnItemInserted="dvCustomer_ItemInserted" OnItemUpdated="dvCustomer_ItemUpdated">
                    <Fields>
                        <asp:TemplateField HeaderText="Email" SortExpression="Email">
                            <EditItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <div class="col-xs-11 col-align">
                                    <asp:TextBox ID="txtEmail" runat="server" Text='<%# Bind("Email") %>' MaxLength="25" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="The Email is required" ControlToValidate="txtEmail" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>

                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="LastName" SortExpression="LastName">
                            <EditItemTemplate>
                                <div class="col-xs-11 col-align">
                                    <asp:TextBox ID="txtLastName" runat="server" Text='<%# Bind("LastName") %>' MaxLength="20" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="The Last Name is required" ControlToValidate="txtLastName" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>

                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <div class="col-xs-11 col-align">
                                    <asp:TextBox ID="txtLastName" runat="server" Text='<%# Bind("LastName") %>' MaxLength="20" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="The Last Name is required" ControlToValidate="txtLastName" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>

                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FirstName" SortExpression="FirstName">
                            <EditItemTemplate>
                                <div class="col-xs-11 col-align">
                                    <asp:TextBox ID="txtFirstName" runat="server" Text='<%# Bind("FirstName") %>' MaxLength="20" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="The First Name is required" ControlToValidate="txtFirstName" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>

                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <div class="col-xs-11 col-align">
                                    <asp:TextBox ID="txtFirstName" runat="server" Text='<%# Bind("FirstName") %>' MaxLength="20" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="The First Name is required" ControlToValidate="txtFirstName" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>

                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Address" SortExpression="Address">
                            <EditItemTemplate>
                                <div class="col-xs-11 col-align">
                                    <asp:TextBox ID="txtAddress" runat="server" Text='<%# Bind("Address") %>' MaxLength="40" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="The Address is required" ControlToValidate="txtAddress" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>

                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <div class="col-xs-11 col-align">
                                    <asp:TextBox ID="txtAddress" runat="server" Text='<%# Bind("Address") %>' MaxLength="40" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="The Address is required" ControlToValidate="txtAddress" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>

                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="City" SortExpression="City">
                            <EditItemTemplate>
                                <div class="col-xs-11 col-align">
                                    <asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("City") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="The City is required" ControlToValidate="txtCity" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>

                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <div class="col-xs-11 col-align">
                                    <asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("City") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="The City is required" ControlToValidate="txtCity" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>

                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("City") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="State" SortExpression="State">
                            <EditItemTemplate>
                                <div class="col-xs-11 col-align">
                                    <asp:DropDownList ID="ddlStates" runat="server" DataSourceID="SqlDataSource3" DataTextField="StateName" DataValueField="StateCode" SelectedValue='<%# Bind("State") %>' CssClass="form-control" ></asp:DropDownList>

                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="The State is required" ControlToValidate="ddlStates" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>

                            </EditItemTemplate>
                            <InsertItemTemplate>
                                
                                <div class="col-xs-11 col-align">
                                    <asp:DropDownList ID="ddlStates" runat="server" DataSourceID="SqlDataSource3" DataTextField="StateName" DataValueField="StateCode" SelectedValue='<%# Bind("State") %>' CssClass="form-control" ></asp:DropDownList>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="The State is required" ControlToValidate="ddlStates" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>

                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("State") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ZipCode" SortExpression="ZipCode">
                            <EditItemTemplate>
                                <div class="col-xs-11 col-align">
                                    <asp:TextBox ID="txtZipCode" runat="server" Text='<%# Bind("ZipCode") %>' MaxLength="9" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="The Zip Code is required" ControlToValidate="txtZipCode" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>

                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <div class="col-xs-11 col-align">
                                    <asp:TextBox ID="txtZipCode" runat="server" Text='<%# Bind("ZipCode") %>' MaxLength="9" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="The Zip Code is required" ControlToValidate="txtZipCode" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>

                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%# Bind("ZipCode") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PhoneNumber" SortExpression="PhoneNumber">
                            <EditItemTemplate>
                                <div class="col-xs-11 col-align">
                                    <asp:TextBox ID="txtPhone" runat="server" Text='<%# Bind("PhoneNumber") %>' MaxLength="20" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="The Phone Number is required" ControlToValidate="txtPhone" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>

                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <div class="col-xs-11 col-align">
                                    <asp:TextBox ID="txtPhone" runat="server" Text='<%# Bind("PhoneNumber") %>' MaxLength="20" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="The Phone Number is required" ControlToValidate="txtPhone" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>

                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%# Bind("PhoneNumber") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" ItemStyle-BackColor="Gray" ItemStyle-ForeColor="White" />
                    </Fields>
                </asp:DetailsView>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [Customers] WHERE [Email] = @original_Email AND [LastName] = @original_LastName AND [FirstName] = @original_FirstName AND [Address] = @original_Address AND [City] = @original_City AND [State] = @original_State AND [ZipCode] = @original_ZipCode AND [PhoneNumber] = @original_PhoneNumber" InsertCommand="INSERT INTO [Customers] ([Email], [LastName], [FirstName], [Address], [City], [State], [ZipCode], [PhoneNumber]) VALUES (@Email, @LastName, @FirstName, @Address, @City, @State, @ZipCode, @PhoneNumber)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT [Email], [LastName], [FirstName], [Address], [City], [State], [ZipCode], [PhoneNumber] FROM [Customers] WHERE ([Email] = @Email)" UpdateCommand="UPDATE [Customers] SET [LastName] = @LastName, [FirstName] = @FirstName, [Address] = @Address, [City] = @City, [State] = @State, [ZipCode] = @ZipCode, [PhoneNumber] = @PhoneNumber WHERE [Email] = @original_Email AND [LastName] = @original_LastName AND [FirstName] = @original_FirstName AND [Address] = @original_Address AND [City] = @original_City AND [State] = @original_State AND [ZipCode] = @original_ZipCode AND [PhoneNumber] = @original_PhoneNumber">
                    <DeleteParameters>
                        <asp:Parameter Name="original_Email" Type="String" />
                        <asp:Parameter Name="original_LastName" Type="String" />
                        <asp:Parameter Name="original_FirstName" Type="String" />
                        <asp:Parameter Name="original_Address" Type="String" />
                        <asp:Parameter Name="original_City" Type="String" />
                        <asp:Parameter Name="original_State" Type="String" />
                        <asp:Parameter Name="original_ZipCode" Type="String" />
                        <asp:Parameter Name="original_PhoneNumber" Type="String" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Email" Type="String" />
                        <asp:Parameter Name="LastName" Type="String" />
                        <asp:Parameter Name="FirstName" Type="String" />
                        <asp:Parameter Name="Address" Type="String" />
                        <asp:Parameter Name="City" Type="String" />
                        <asp:Parameter Name="State" Type="String" />
                        <asp:Parameter Name="ZipCode" Type="String" />
                        <asp:Parameter Name="PhoneNumber" Type="String" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="grdCustomers" Name="Email" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="LastName" Type="String" />
                        <asp:Parameter Name="FirstName" Type="String" />
                        <asp:Parameter Name="Address" Type="String" />
                        <asp:Parameter Name="City" Type="String" />
                        <asp:Parameter Name="State" Type="String" />
                        <asp:Parameter Name="ZipCode" Type="String" />
                        <asp:Parameter Name="PhoneNumber" Type="String" />
                        <asp:Parameter Name="original_Email" Type="String" />
                        <asp:Parameter Name="original_LastName" Type="String" />
                        <asp:Parameter Name="original_FirstName" Type="String" />
                        <asp:Parameter Name="original_Address" Type="String" />
                        <asp:Parameter Name="original_City" Type="String" />
                        <asp:Parameter Name="original_State" Type="String" />
                        <asp:Parameter Name="original_ZipCode" Type="String" />
                        <asp:Parameter Name="original_PhoneNumber" Type="String" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [StateCode], [StateName] FROM [States] ORDER BY [StateName]"></asp:SqlDataSource>
                <p><asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                        HeaderText="Please correct the following errors:" 
                        CssClass="text-danger" />
                <p><asp:Label ID="lblError" runat="server" 
                    EnableViewState="false" CssClass="text-danger"></asp:Label></p>

            </div>

        </form>
    </main>
</div>
</body>
</html>