package dbResources;
/*
 * Name: Maxwell McElhone
 * Date: 10/22/2018
 * Program: Chapter 20 Exercise 1
 */
public class DBException extends Exception{
	public DBException() {}
	
	public DBException(Exception cause){
		super(cause);
	}
}
