package dbResources;
/*
 * Name: Maxwell McElhone
 * Date: 10/22/2018
 * Program: Chapter 20 Exercise 1
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductDB {

	///returns all rows of the database table
	public static List<Product> getAll() throws DBException{
		String sql = "SELECT * FROM products ORDER BY ProductId";
		List<Product> products = new ArrayList<>();
		Connection connection = DBUtil.getConnection();
		try (PreparedStatement ps = connection.prepareStatement(sql);
				 ResultSet rs = ps.executeQuery()) {
			while (rs.next()) {
				int productID = rs.getInt("ProductId");
				String code = rs.getString("ProductCode");
				String description = rs.getString("Description");
				Double price = rs.getDouble("Price");

				Product p = new Product();
				p.setId(productID);
				p.setCode(code);
				p.setDescription(description);
				p.setPrice(price);
				products.add(p);
			}
			return products;
		} catch (SQLException e) {
			throw new DBException(e);
		}
	}

	// this returns only one product searched for by the code
	public static Product get(String productCode) throws DBException{
		String sql = "SELECT * FROM products WHERE ProductCode = ?";
		Connection connection = DBUtil.getConnection();

		try(PreparedStatement ps = connection.prepareStatement(sql)){
			ps.setString(1, productCode);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				int productID = rs.getInt("ProductId");
				String code = rs.getString("ProductCode");
				String description = rs.getString("Description");
				Double price = rs.getDouble("Price");
				rs.close();

				Product p = new Product();
				p.setId(productID);
				p.setCode(code);
				p.setDescription(description);
				p.setPrice(price);


				return p;
			} else{
				rs.close();
				return null;
			}
		} catch (SQLException e){
			throw new DBException(e);
		}

	}

	// this added the product to the database
	public static void add(Product product) throws DBException{
		String sql = "INSERT INTO products (ProductCode, Description, Price) "
								+ "VALUES (?, ?, ?)";
		Connection connection = DBUtil.getConnection();
		try (PreparedStatement ps = connection.prepareStatement(sql)){
			ps.setString(1, product.getCode());
			ps.setString(2, product.getDescription());
			ps.setDouble(3, product.getPrice());
			ps.executeUpdate();
		}
		catch (SQLException e) {
			throw new DBException(e);
		}
	}

	// this updates the selected product in the database
	public static void update(Product product) throws DBException{
		String sql = "UPDATE products SET ProductCode = ?, Description = ?, " +
								"Price = ? WHERE ProductId = ?";
		Connection connection = DBUtil.getConnection();
		try (PreparedStatement ps = connection.prepareStatement(sql)){
			ps.setString(1, product.getCode());
			ps.setString(2, product.getDescription());
			ps.setDouble(3, product.getPrice());
			ps.setInt(4, product.getId());
			ps.executeUpdate();
		}
		catch (SQLException e) {
			throw new DBException(e);
		}
	}

	// this deletes the selected product in the database
	public static void delete(Product product) throws DBException{
		String sql = "DELETE FROM products WHERE ProductId = ?";
		Connection connection = DBUtil.getConnection();
		try(PreparedStatement ps = connection.prepareStatement(sql)){
			ps.setInt(1, product.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DBException(e);
		}
	}
}
