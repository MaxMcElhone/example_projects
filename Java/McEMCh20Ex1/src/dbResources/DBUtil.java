package dbResources;
/*
 * Name: Maxwell McElhone
 * Date: 10/22/2018
 * Program: Chapter 20 Exercise 1
 */
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;

public class DBUtil {
	
	private static Connection connection;
	
	private DBUtil(){}
	
	// get connected to the database
	public static synchronized Connection getConnection() throws DBException{
		if(connection != null){
			return connection;
		}
		else{
			try	{
				// set the db url, username, and password
				String dbURL = "jdbc:mysql://localhost:3306/mma";
				String username = "root";
				String password = "";
				
				// get and return connection
				Class.forName("com.mysql.jdbc.Driver");
				connection = DriverManager.getConnection(dbURL, username, password);
				return connection;
			}catch(SQLException|ClassNotFoundException e){
				throw new DBException(e);
			}
		}
	}
	
	// closes the connection
	public static synchronized void closeConnection() throws DBException{
		if(connection != null){
			try{
				connection.close();
			}catch(SQLException e){
				throw new DBException(e);
			}finally{
				connection = null;
			}
		}
	}
	
}
