<!DOCTYPE HTML><%@page language="java" import="dbResources.*, java.util.ArrayList, java.util.List"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!--
	Name: Maxwell McElhone
	Date: 10/22/2018
	Program: Chapter 20 Exercise 1
  -->
<html>
<head>
<title>index</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark text-light">
		<a class="navbar-brand" href="index.jsp">Chapter 20 Ex 1</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="index.jsp">Home <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="add.html">Add a Product</a>
				</li>
			</ul>
		</div>
	</nav>
	<div class="container text-center">
<%
	List<Product> products = null;
	// try to get all of the products
	try {
		products = ProductDB.getAll();
	}
	catch(DBException e){
		out.print("Error<br />" + e.getMessage());
	}
	
	// if products were returned
	if(products != null){
		int indexer = 0;
		//create a deck (like a row)
		out.print("<div class='card-deck mt-2'>");
		for (Product p : products ) {
			if(indexer == 3){
				out.print("</div><div class='card-deck mt-2'>");
				indexer = 0;
			}
			// fill it with values from the objects with forms that direct to the 
			// edit page while passing the code of the product
			out.print("<div class='card bg-light'><form action='edit.jsp' class='card-body' method='get'><h3>" +
				p.getCode() + "</h3>" + "<p>" + p.getDescription() + "</p><p>" +
				p.getPriceFormatted() + "</p>");
				out.print("<input type='text' name='code' class='d-none' value='" + p.getCode() + "' />");
				out.print("<input name='Edit' type='submit' class='btn btn-info' value='Edit'/></form></div>");

				indexer++;
			}
			out.print("</div>");
		}
		else{
			out.print("No products returned.");
		}
%>
	</div>
</body>
</html>
