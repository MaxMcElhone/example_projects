<!DOCTYPE HTML><%@page language="java" import="dbResources.*"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!--
	Name: Maxwell McElhone
	Date: 10/22/2018
	Program: Chapter 20 Exercise 1
  -->
<html>
<head>
<title>Output</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark text-light">
		<a class="navbar-brand" href="index.jsp">Chapter 20 Ex 1</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="index.jsp">Home <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="add.html">Add a Product</a>
				</li>
			</ul>
		</div>
	</nav>
  <div class="container text-center bg-light">
    <h2 class="pt-3 pb-3 mr-auto ml-auto">
    <%
      //get what action is being called
      String action = request.getParameter("action");
      // create a product
      Product p = new Product();
		
	  // set all of the products properties
      p.setCode(request.getParameter("code"));
      p.setDescription(request.getParameter("description"));
      p.setPrice(Double.parseDouble(request.getParameter("price")));
      String idIn = request.getParameter("id");

	  // if an id is passed
      if(idIn != null){
        p.setId(Integer.parseInt(idIn));
      }

	  // if an update was called
      if(action.equalsIgnoreCase("update")){
      	//try to update
        try {
          ProductDB.update(p);
          out.print("Update completed successfully.");
        } catch(DBException e) {
          out.print("A error occured while updating product <br />");
          out.print(e.getMessage());
        }
      }
      //if a delete was called
      else if (action.equalsIgnoreCase("delete")) {
      	// try to delete
        try {
          ProductDB.delete(p);
          out.print("Delete completed successfully.");
        } catch(DBException e) {
          out.print("A error occured while deleting product <br />");
          out.print(e.getMessage());
        }
      }
      // if an insert was called
      else if (action.equalsIgnoreCase("insert")) {
      	// try to insert
        try {
          ProductDB.add(p);
          out.print("Insert completed successfully.");
        } catch(DBException e) {
          out.print("A error occured while inserting product <br />");
          out.print(e.getMessage());
        }
      }
      // if something else was called or nothing was called at all
      else {
        out.print("an error has occured.");
      }
    %>
    </h2>
    <a href="index.jsp" class="btn btn-secondary mt-2 mb-5">Home</a>
  </div>
</body>
</html>
