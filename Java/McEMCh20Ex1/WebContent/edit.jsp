<!DOCTYPE HTML><%@page language="java" import="dbResources.*"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!--
	Name: Maxwell McElhone
	Date: 10/22/2018
	Program: Chapter 20 Exercise 1
  -->
<html>
<head>
<title>Edit</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark text-light">
		<a class="navbar-brand" href="index.jsp">Chapter 20 Ex 1</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="index.jsp">Home <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="add.html">Add a Product</a>
				</li>
			</ul>
		</div>
	</nav>
	<div class="container text-center bg-light">
    <div class='col-10 ml-auto mr-auto'>
      <div class="heading">
        <h2 class="pt-3 pb-3">Edit Product</h2>
      </div>
      <form action="output.jsp" method="get">
  <%
  	//create a product
    Product p = null;
    
    // get the code passed
    String code = request.getParameter("code");
    
    // try to find it in the database
    try {
  		p = ProductDB.get(code);
  	}
  	catch(DBException e){
  	
  		//output an error
  		out.print("Error<br />" + e.getMessage());
  	}

	//if something was found
    if (p != null) {
    
      //get the description
      String description = p.getDescription();
      
	  // replace all single and double quotes with html equivalents
	  // otherwise not everything is outputted to the screen
      for (int i = 0; i < description.length(); i++)  {
        
        description = description.replace("'", "&#39;");
        description = description.replace("\"", "&#34;");
      }
      
      //cteate a div and input tags filled with all the information from the database
      out.print("<div class='input-group pb-3'><div class='input-group-prepend'>"+
                "<span class='input-group-text'> ID </span></div>" +
                "<input type='text' name='id' class='form-control' value='" + p.getId() +
                "' readonly></div>");
      out.print("<div class='input-group pb-3'><div class='input-group-prepend'>"+
                "<span class='input-group-text'> Code </span></div>" +
                "<input type='text' name='code' class='form-control' value='" + p.getCode() +
                "' required></div>");
      out.print("<div class='input-group pb-3'><div class='input-group-prepend'>"+
                "<span class='input-group-text'> Description </span></div>" +
                "<input type='text' name='description' class='form-control' value='" + description +
                "' required></div>");
      out.print("<div class='input-group pb-3'><div class='input-group-prepend'>"+
                "<span class='input-group-text'> Price </span></div>" +
                "<input type='number' name='price' step='0.01' class='form-control' value='" + p.getPrice() +
                "' required></div>");
                
      //create some buttons to update or delete the object
      out.print("<input type='submit' name='action' value='Update' class='btn btn-success mr-2 mb-3 mt-2' />");
      out.print("<input type='submit' name='action' value='Delete' class='btn btn-danger mb-3 mt-2' />");
    }
    // if nothing was found
    else{
      out.print("Not product returned.");
    }
  %>
      </form>
    </div>
  </div>
</body>
</html>
