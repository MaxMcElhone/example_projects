/**
 * 
 */
package murach.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import murach.business.Product;
import murach.db.DBException;
import murach.db.ProductDB;


/**
 * @author Maxwell.McElhone
 * Date: 11/2/2018
 * Program: Chapter 21 Exercise 1
 */
@SuppressWarnings("serial")
public class ProductForm extends JDialog {
	// All the fields on the form
	// might add a way to display the ID
	private JTextField codeField;
	private JTextField descriptionField;
	private JTextField priceField;
	private JButton btnConfirm;
	private JButton btnCancel;
	
	// product that is being updated or created.
	private Product p = new Product();
	
	// base constructor
	public ProductForm(java.awt.Frame parent, String title, boolean modal) {
		super(parent, title, modal);
		initComponents();
	}
	
	//If this is an update
	public ProductForm(java.awt.Frame parent, String title, boolean modal, Product product){
		this(parent, title, modal);
		this.p = product;
		btnConfirm.setText("Save");
		codeField.setText(p.getCode());
		descriptionField.setText(p.getDescription());
		priceField.setText(Double.toString(p.gePrice()));
	}
	
	//create the initialize the components and add them to the form
	private void initComponents(){
		codeField = new JTextField();
		descriptionField = new JTextField();
		priceField = new JTextField();
		btnCancel = new JButton();
		btnConfirm = new JButton();
		
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		Dimension shortField = new Dimension(100, 20);
		Dimension longField = new Dimension(300, 20);
		codeField.setPreferredSize(shortField);
		codeField.setMinimumSize(shortField);
		priceField.setPreferredSize(shortField);
		priceField.setMinimumSize(shortField);
		descriptionField.setPreferredSize(longField);
		descriptionField.setMinimumSize(longField);
		
		btnCancel.setText("Cancel");
		btnCancel.addActionListener((ActionEvent) -> {
			cancelButtonActionPerformed();
		}); 
		
		btnConfirm.setText("Add");
		btnConfirm.addActionListener((ActionEvent) -> {
			confirmButtonActionPerformed();
		});
		
		// JLabel and JTextField panel
		JPanel productPanel = new JPanel();
		productPanel.setLayout(new GridBagLayout());
		productPanel.add(new JLabel("Code:"), getConstraints(0, 0, GridBagConstraints.LINE_END));
		productPanel.add(codeField, getConstraints(1, 0, GridBagConstraints.LINE_START));
		productPanel.add(new JLabel("Description:"), getConstraints(0, 1, GridBagConstraints.LINE_END));
		productPanel.add(descriptionField, getConstraints(1, 1, GridBagConstraints.LINE_START));
		productPanel.add(new JLabel("Price:"), getConstraints(0, 2, GridBagConstraints.LINE_END));
		productPanel.add(priceField, getConstraints(1, 2, GridBagConstraints.LINE_START));
		
		// JButton panel
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(btnConfirm);
		buttonPanel.add(btnCancel);
		
		// Add panels to main panel
		setLayout(new BorderLayout());
		add(productPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		pack();
	}
	
	// method for placing the items onto the form
	private GridBagConstraints getConstraints(int x, int y, int anchor){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(5,5,0,5);
		c.gridx = x;
		c.gridy = y;
		c.anchor = anchor;
		return c;
	}
	
	private void cancelButtonActionPerformed(){
		dispose();
	}
	
	// if every thing entered is valid update or add.
	private void confirmButtonActionPerformed(){
		if (validateData()){
			setData();
			if (btnConfirm.getText().equals("Add")){
				doAdd();
			} else {
				doEdit();
			}
		}
	}
	
	// validate the data in the input fields
	private boolean validateData(){
		String code = codeField.getText();
		String name = descriptionField.getText();
		String priceString = priceField.getText();
		
		if (code == null || name == null || priceString == null || code.isEmpty() || name.isEmpty() || priceString.isEmpty()){
			JOptionPane.showMessageDialog(this, 
					"Please fill in all fields.", 
					"MissingFields", 
					JOptionPane.INFORMATION_MESSAGE);
			return false;
		} else {
			try {
				@SuppressWarnings("unused")
				double price = Double.parseDouble(priceString);
			} catch (NumberFormatException e){
				JOptionPane.showMessageDialog(this, 
						"The price field is invalid", 
						"Invalid Price", 
						JOptionPane.INFORMATION_MESSAGE);
				priceField.requestFocusInWindow();
				return false;
			}
		}
		return true;
	}
	
	// assign the data in the input fields to the product
	private void setData(){
		String code = codeField.getText();
		String description = descriptionField.getText();
		String priceString = priceField.getText();
		double price = Double.parseDouble(priceString);
		p.setCode(code);
		p.setDescription(description);
		p.setPrice(price);
	}
	
	// attempt an update using the product
	private void doEdit(){
		try {
			ProductDB.update(p);
			dispose();
			fireDBUpdateEvent();
		} catch (DBException e){
			JOptionPane.showMessageDialog(this, e);
		}
	}
	
	// attempt an insert using the product
	private void doAdd(){
		try {
			ProductDB.add(p);
			dispose();
			fireDBUpdateEvent();
		} catch (DBException e){
			JOptionPane.showMessageDialog(this, e);
		}
	}
	
	// update the main form.
	private void fireDBUpdateEvent(){
		ProductManagerFrame mainWindow = (ProductManagerFrame) getOwner();
		mainWindow.fireDBUpdatedEvent();
	}
}
