package murach;

import murach.ui.ProductManagerFrame;

/**
 * @author Maxwell.McElhone
 * Date: 11/2/2018
 * Program: Chapter 21 Exercise 1
 */
public class Main {

    @SuppressWarnings("unused")
	public static void main(String[] args) {
        ProductManagerFrame frame = new ProductManagerFrame();
    }
}