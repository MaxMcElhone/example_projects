<?php
include('class_lib/StudentsDB_Access.php');

$DB_Access = new StudentsDB_Access(); // create a new object from the studentDB_Access class

$DB_Result = $DB_Access->showDatabases();

$rValue = "<h3>List of DATABASES in MySQL:</h3>";

while ($row = $DB_Result->fetch_assoc()) {
    foreach ($row as $value) {
        $rValue = $rValue . "<h4>$value</h4>";
    }
}

print($rValue);

print("<hr />");

$DB_Result = $DB_Access->showTables();

$rValue = "<h3>List of Tables in students: </h3><h4>";

while ($row = $DB_Result->fetch_assoc()) {
    foreach ($row as $value) {
        $rValue = $rValue . "$value ";
    }
}

print($rValue . "</h4>");

print("<hr />");

$table = "mcem_motorcycles"; //set which table to display records FROM

$DB_Result = $DB_Access->displayRecords($table);

$rValue = "<h3>List of Records from " . $table . " table</h3>";

while ($row = $DB_Result->fetch_assoc()) {
    $rValue = $rValue . "<h4>";

    foreach ($row as $value) {
        $rValue = $rValue . "$value &#xb7; ";
    }
    $rValue = $rValue . "</h4>";
}

print($rValue);


 ?>
