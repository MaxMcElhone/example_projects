<?php

header("Content-type: application/json"); // json incoming
include('class_lib/StudentsDB_Access.php'); // database access class
include("class_lib/McEM_Song.php"); // class modeled after database table

$DB_Access = new StudentsDB_Access();

$tableName = $_REQUEST['tableName'];
$method = $_REQUEST['method'];

$result;

if( strcasecmp($method, 'selectOne') == 0){
    $ID = $_REQUEST['ID'];
    $result = $DB_Access->selectOne($tableName, $ID);
}
else if ( strcasecmp($method, 'displayRecords') == 0){
    $result = $DB_Access->displayRecords($tableName);
}

$data = array();

if ( $tableName == "McEM_songs"){
    while ($row = $result->fetch_assoc()) {

        $song = new McEM_Song();

        $song->setId($row['id']);
        $song->setTitle($row['title']);
        $song->setBand($row['band']);
        $song->setLength($row['length']);

        $data[] = (array) $song;
    }

}

print(json_encode($data));

 ?>
