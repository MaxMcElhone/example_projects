<?php

class McEM_Song
{
    private $id = 0;
    private $title = 'Default Title';
    private $band = 'Default Band';
    private $length = "00:00";

    public function getId()
    {
        return $this->id;
    }

    public function setId($aId)
    {
        $this->id = $aId;

        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($aTitle)
    {
        $this->title = $aTitle;

        return $this->title;
    }

    public function getBand()
    {
        return $this->band;
    }

    public function setBand($aBand)
    {
        $this->band = $aBand;

        return $this->band;
    }

    public function getLength()
    {
        return $this->length;
    }

    public function setLength($aLength)
    {
        $this->length = $aLength;

        return $this->length;
    }

    public function toString()
    {
        $msg = "ID: " . $this->getId() . ", Title: " . $this->getTitle() .
                ", Band: " . $this->getBand() . ", Length: " . $this->getLength();
        return $msg;
    }

}

 ?>
